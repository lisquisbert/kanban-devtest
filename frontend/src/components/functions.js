export const isEmail = (email) => {
    var response = {
        message: '',
        bool: ''
    }
    if (email !== '' && email !== null && email !== undefined) {
        let lastAtPos = email.lastIndexOf('@');
        let lastDotPos = email.lastIndexOf('.');
        if (lastAtPos < lastDotPos && lastAtPos > 0 && email.indexOf('@@') === -1 && lastDotPos > 2 && (email.length - lastDotPos) > 2) {
            var before = email.substring(0, lastAtPos);
            if (before.length > 3) {
                response.message = "Correct";
                response.bool = true;
            } else {
                response.message = "Email too short";
                response.bool = false;
            }
        } else {
            response.message = "Invalid email";
            response.bool = false;
        }
    } else {
        response.message = 'Empty field';
        response.bool = false;
    }
    return response
};