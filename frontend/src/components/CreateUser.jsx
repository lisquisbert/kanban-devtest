import React, { Component } from 'react';
import {isEmail} from './functions';

class CreateUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            fullname: '',
            email: '',
            showAlert: false,
            textAlert: ''
        };
    }

    

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });

        switch(name){
            case 'email':
                this.setState({
                    showAlert: true,
                    textAlert: isEmail(value).message
                })
            break;
            default:
                break;
        }
    };

    get isValid() {
        const { username, fullname, email } = this.state;
        return username && fullname && isEmail(email).bool;
    }

    get isInvalid() {
        return !this.isValid;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        
        if (this.isInvalid) return;

        const { onCreateUser, listId } = this.props;

        if (onCreateUser) {
            onCreateUser(listId, this.state);
        }

        this.setState({
            username: '',
            fullname: '',
            email: ''
        });
    };

    render() {
        const { username, fullname, email } = this.state;

        return (
        <form className="CreateUser" onSubmit={this.handleSubmit}>
            
            <input
                className="CreateUser-username"
                onChange={this.handleChange}
                name="username"
                placeholder="username"
                type="text"
                value={username}
            />
            <input
                className="CreateUser-fullname"
                onChange={this.handleChange}
                placeholder="fullname"
                name="fullname"
                type="text"
                value={fullname}
            />
            {this.state.showAlert? <div>{this.state.textAlert}</div>:null}
            <input
                className="CreateUser-email"
                onChange={this.handleChange}
                placeholder="email"
                name="email"
                type="text"
                value={email}
            />
            <input
                className="CreateUser-submit"
                type="submit"
                value="Create User"
                disabled={this.isInvalid}
            />
        </form>
        );
    }
}

export default CreateUser;
