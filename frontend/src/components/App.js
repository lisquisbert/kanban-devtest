import React, { Component } from 'react';
import CreateUser from './CreateUser';
import CreateWorkItem from './CreateWorkItem';
import Lists from './Lists';
import ListUsers from './ListUsers';

class Application extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      defaultList: 0, 
      error: null, 
      isLoaded: false, 
      lists: [],
      defaultListUser: [],    //0
      errorUser: null, 
      isLoadedUser: false, 
      listsUser: []
    };
  }

  createWorkItem = (kanbanListId, { title, description }) => {
    const newWorkItem = {
      title,
      description,
      kanbanListId,
    };

    fetch(`${process.env.REACT_APP_KANBAN_BOARD_API}/workitems`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newWorkItem),
    })
      .then((res) => res.json())
      .then(
        () => {
          this.fetchKanbanLists();
        },
        (error) => {
          console.log('Error on create: ', error);
        }
      );
  };

  updateWorkItem = async (targetListId, { workItemId, title, description }) => {
    const workItemUpdated = {
      title,
      description,
      kanbanListId: targetListId,
    };

    try {
      const response = await fetch(
        `${process.env.REACT_APP_KANBAN_BOARD_API}/workitems/${workItemId}`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(workItemUpdated),
        }
      );

      if (response) {
        this.fetchKanbanLists();
      }
    } catch (err) {
      console.error('Error on update: ', err);
    }
  };

  deleteWorkItem = (workItemId) => {
    fetch(`${process.env.REACT_APP_KANBAN_BOARD_API}/workitems/${workItemId}`, {
      method: 'DELETE',
    }).then(
      () => {
        this.fetchKanbanLists();
      },
      (error) => {
        console.log('Error on delete: ', error);
      }
    );
  };

  fetchKanbanLists = () => {
    fetch(`${process.env.REACT_APP_KANBAN_BOARD_API}/kanbanlists`)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            lists: result,
            defaultList: result[0].kanbanListId,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  };

  componentDidMount() {
    this.fetchKanbanLists();
    this.fetchUsers();
  }

  createUser = (userListId, { username, fullname, email }) => {
    const newUser = {
      username,
      fullname,
      email,
      userListId,
    };

  fetch(`${process.env.REACT_APP_KANBAN_BOARD_API}/api/users`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newUser),
    })
      .then((res) => res.json())
      .then(
        () => {
          this.fetchUsers();
        },
        (error) => {
          console.log('Error on create: ', error);
        }
      );

      /*Added because there is not an API */
      /*var listOfUsers = this.state.defaultListUser;
      listOfUsers.push(newUser);
      this.setState({
        listsUser: listOfUsers
      });*/
      //this.fetchUsers();
  };

  fetchUsers = () => {

    fetch(`${process.env.REACT_APP_KANBAN_BOARD_API}/api/users`)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoadedUser: true,
            listsUser: result,
            defaultListUser: result[0].kanbanListId,
          });
        },
        (errorUser) => {
          this.setState({
            isLoadedUser: true,
            errorUser,
          });
        }
      );

      //this.setState({
        //isLoadedUser: true,
        //listsUser: this.state.listsUser
        //defaultListUser: result[0].kanbanListId,
     // });
  };

  updateUser = async (targetListId, { userId, username, fullname, email }) => {
    const userUpdated = {
      username,
      fullname,
      email,
      kanbanListId: targetListId,
    };

    try {
      const response = await fetch(
        `${process.env.REACT_APP_KANBAN_BOARD_API}/api/users/${username}`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(userUpdated),
        }
      );

      if (response) {
        this.fetchUsers();
      }
    } catch (err) {
      console.error('Error on update: ', err);
    }
  };

  deleteUser = (username) => {
    fetch(`${process.env.REACT_APP_KANBAN_BOARD_API}/api/users/${username}`, {
      method: 'DELETE',
    }).then(
      () => {
        this.fetchUsers();
      },
      (error) => {
        console.log('Error on delete: ', error);
      }
    );
  };

  render() {
    const { error, isLoaded, lists } = this.state;

    if (error) {
      return <div className="error">Error: {error.message}</div>;
    }

    if (!isLoaded) {
      return <div className="loading">Loading...</div>;
    }

    return (
      <>
        <header className="Header">
          <h1>Users and Kanban Board</h1>
          <section>
            <CreateUser 
              onCreateUser={this.createUser}
              listIdUsers={this.state.defaultListUsers} />
          </section>
          <section>
            <ListUsers users={this.state.listsUser}
                        onDeleteUser={this.deleteUser}
                        onUpdateUser={this.updateUser} />
          </section>
        </header>
        <main className="Application">
          <section>
            <CreateWorkItem
              onCreateWorkItem={this.createWorkItem}
              listId={this.state.defaultList}
            />
          </section>
          <section>
            <Lists
              lists={lists}
              onDeleteWorkItem={this.deleteWorkItem}
              onUpdateWorkItem={this.updateWorkItem}
            />
          </section>
        </main>
        <footer className="Footer">
          <h2>Jalasoft DevTest 2020</h2>
        </footer>
      </>
    );
  }
}

export default Application;
