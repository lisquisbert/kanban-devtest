import React, { Component } from 'react';
import {isEmail} from './functions';

class User extends Component {
    state = { showOptions: false, listIdUsers: '', editMode: false, fullname: '', email: '', };

    handleChange = (event) => {
        this.setState({
            listIdUsers: event.target.value,
        });
    };

    componentDidMount = () => {
        const { listIdUsers } = this.props;
        this.setState({
            listIdUsers,
        });
    };

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });

        switch(name){
            case 'email':
                this.setState({
                    showAlert: true,
                    textAlert: isEmail(value).message
                })
            break;
            default:
                break;
        }
    };

    render() {
        const {
            user = {},
            onDeleteUser,
            onUpdateUser,
        } = this.props;
        const { listIdUsers } = this.state;

        const saveUser = () => {
            
            var updatedUser = {
                username: user.username,
                fullname: this.state.fullname,
                email: this.state.email
            }

            const newListIdUsers = +listIdUsers;
                onUpdateUser(newListIdUsers, updatedUser);
            
            this.setState({
                editMode: false
            });
        };

        const updateMode = () => {
            this.setState({ editMode: true });
        };

        const deleteUser = () => {
            const { username } = user;
            onDeleteUser(username);
        };

        return (
        <article className="CreateUser">
            <h3>{user.username}</h3>
            {!this.state.editMode ? <div className="CreateUser-fullname">{user.fullName}</div>:
                <input
                    className="CreateUser-fullname"
                    onChange={this.handleChange}
                    placeholder="fullname"
                    name="fullname"
                    type="text"
                    defaultValue={user.fullName}
                />
            }
            {!this.state.editMode ? <div className="CreateUser-email">{user.email}</div> :
                <input
                    className="CreateUser-email"
                    onChange={this.handleChange}
                    placeholder="email"
                    name="email"
                    type="text"
                    defaultValue={user.email}
                />
            }

            <div className="CreateUser-options">
            {!this.state.editMode ?
                <button
                    type="submit"
                    onClick={updateMode}
                    className="CreateUser-button info"
                >
                    EDIT
                </button>
            :
            <div>
                <button
                    type="submit"
                    onClick={saveUser}
                    className="CreateUser-button info"
                >
                    UPDATE
                </button>
                <button onClick={deleteUser} className="CreateUser-button delete">
                    DELETE
                </button>
            </div>
            }
            </div>
        </article>
        );
    }
}

export default User;
