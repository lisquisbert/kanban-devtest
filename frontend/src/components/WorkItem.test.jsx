import React from 'react';
import { render } from '@testing-library/react';
import WorkItem from './WorkItem';

it('renders save button', () => {
  const { getByText } = render(<WorkItem />);
  expect(getByText('SAVE')).toBeInTheDocument();
});