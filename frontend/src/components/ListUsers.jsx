import React, { Component } from 'react';

import User from './User';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = { showOptions: false };
    }

    toggleOptions = () => {
        this.setState(({ showOptions }) => ({ showOptions: !showOptions }));
    };

    render() {
        const { list = {}, lists, onDeleteUser, onUpdateUser } = this.props;
        const users = this.props.users;
        const hasUsers = users.length > 0;
        return (
        <article className="ListUser">
            {!hasUsers && (
                <p className="List-info">There are no users in this list</p>
            )}
            <div>
            {users.map((user) => (
                <User
                    key={user.id}
                    user={user}
                    listId={list.kanbanListId}
                    lists={lists}
                    onDeleteUser={onDeleteUser}
                    onUpdateUser={onUpdateUser}
                />
            ))}
            </div>
        </article>
        );
    }
}

export default List;
