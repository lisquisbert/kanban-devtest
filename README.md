# Jalasoft, 1st Nationwide Bootcamp

This repository contains the base for the challenges of this bootcamp

## Introduction

This is a Kanban board application to manage work items (similar to Trello)
The code is divide in two parts:

* The frontend, a REACT application that lives in the 'frontend' folder.
* The backend, a DotNet Core application that lives in the 'backend' folder.

For more details please take a look in the internal README.md file in every folder.

## Challenges

We have 2 main features to work it on. Each feature has challenges to solve.

## USERS MANAGEMENT FEATURE

![image](static/img/Kanban-Mockup-Users.jpg)

### ***CREATE USER***
![image](static/img/create-user.jpg)
### Challenge 1 [FrontEnd]
* Create a form to add new users.
  * The form must have 3 fields:
    * Username [String type, required field].
    * Full name [String type, required field].
    * Email [String type, validate if email is valid, required field].
  * After submitting the form:
    * All the required field must be validate it.
      * Show validation errors if one or more fields are invalid.
    * The user should be created.
    * The new user should be displayed inside the users list (below to the form).
    * All the form fields must be clean.

### Challenge 2 [BackEnd]
* Create a new endpoint to create a user.
  * The user must have 3 fields:
    * Username [String type, required field].
    * Full name [String type, required field].
    * Email [String type, validate if the email is valid, required field].
  * Returns an error if any of the required fields are missing.
     
### ***GET USERS***
![image](static/img/show-users.jpg)
### Challenge 3 [FrontEnd]
* Create a list to render all the users.
* Display the username, full name and email.
* Add an `Edit` Button for each user item.

### Challenge 4 [BackEnd]
* Create the resource to get all the users available in the Database.
* Return and empty array in case there is no user.

### ***UPDATE USER***
![update a user](/static/img/update-user.jpg)

### Challenge 5 [FrontEnd]
* A user can be updated clicking on the `EDIT`, to enable `edit mode`, means that the `EDIT` button should be hidden and show the buttons for `UPDATE` and `DELETE`.
* The value of the fields `Full name` and `email` should be shown inside input fields.
  * Only the fields `Full name` and `email` would be able to be updated.
* Update user info after click in `UPDATE` and return the previous mode (`show mode`)

### Challenge 6 [Backend]
* Create the resource to update a user.
* Validate the fields (the same validations as the creation step).
* Returns an error if necessary.

### ***DELETE USER***
![update a user](/static/img/update-user.jpg)
### Challenge 7 [FrontEnd]
* After click on the `DELETE` button inside a user-item the user must be deleted and removed from the user list (Also, this user must be removed from any work item assigned to it).

### Challenge 8 [BackEnd]
* Create the resource for deleting a user.
* Delete the user from the Database.

### ***ASSIGN A USER TO A WORK ITEM***
* A user can be assigned to one or more work items. 
* A work item can have one or more users assigned to it.

![image](static/img/assigned-user-to-work-item.jpg)

### Challenge 9 [FrontEnd]
* Render a new dropdown list with all the users available.
* The button `ADD USER` should assign the selected user to the work item.
  * The selected user shoud be removed from the user list to select.
  * A request to assign the user should be trigger to the backend after clicking on the `ADD USER` button
    * NOTE: Take into account that the `UPDATE` button inside the work item should be responsible only to move the work item to another column (shouldn't be responsable to save the assigned user)
* Show a list of all the user assigned to a work item.
* If all the current users are assigned to a work item, the dropdown list shouldn't be displayed.
* If no user was assigned to the work item, the list of assigned users shouldn't be displayed.

### Challenge 10 [BackEnd]
* Add the logic to assing one or more users to a work item.
* All the users assigned to a work item must be persisted.

### Work Item Tags

![image](static/img/work-item-tags.jpg)

## WORK ITEMS TAGS FEATURE

### ***CREATE TAG***
### Challenge 11 [FrontEnd]
* Each work item should have a text field to add a tag.
* After click on `ADD TAG` button the tags should be assigned to the work item.
  * A request to add the tag should be triggered after clicking the button. 
    * NOTE: Take into account that the `UPDATE` button inside the work item should be responsible only to move the work item to another column (shouldn't be responsable to save the tags)

### Challenge 12 [Backend]
* Create Tags endpoint in order to save all the tags for a specific work item.
* Each work item must have a maximun of 5 tags.

### ***DELETE TAG***
### Challenge 13 [FrontEnd]
* Each Tag can be deleted clicking in the `(x)` button (next to the tag name)
.
### Challenge 14 [BackEnd]
* Create the logic for delete a tag from a especific work item.

Good luck!

## FAQ (Frequently Asked Questions)

* [Cloning the Code](docs/cloning.md)
* [How build and run the projects](docs/building.md)
* [How start with a Challenge](docs/how_take_a_challenge.md)