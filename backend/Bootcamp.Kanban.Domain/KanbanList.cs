﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootcamp.Kanban.Domain
{
    public class KanbanList
    {
        public int KanbanListId { get; set; }
        [Required]
        public string Title { get; set; }
        public int Order { get; set; }

        public virtual ICollection<WorkItem> WorkItems { get; set; }
    }
}
