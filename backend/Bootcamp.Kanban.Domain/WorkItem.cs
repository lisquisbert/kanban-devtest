﻿using System.ComponentModel.DataAnnotations;

namespace Bootcamp.Kanban.Domain
{
    public class WorkItem
    {
        public int WorkItemId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int KanbanListId { get; set; }
    }
}