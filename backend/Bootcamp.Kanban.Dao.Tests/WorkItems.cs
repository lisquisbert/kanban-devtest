using System;
using Bootcamp.Kanban.Dao.Exceptions;
using Bootcamp.Kanban.Domain;
using Xunit;

namespace Bootcamp.Kanban.Dao.Tests
{
    public class WorkItems
    {
        private DatabaseConfiguration Configuration { get; }
        public WorkItems()
        {
            this.Configuration = new DatabaseConfiguration("kanban.test.db");
        }

        private void RecreateDatabase()
        {
            KanbanContext context = new KanbanContext(Configuration);
            context.RecreateDatabase();
        }

        [Fact]
        public void CreateItem()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);

            WorkItem workItem = new WorkItem()
            {
                 Title = "Create integration test",
                 Description = "You need to test the database integration test with the DAO layer.",
                 KanbanListId = 1
            };

            WorkItem responseItem = dao.Create(workItem);
            Assert.NotNull(responseItem);
            Assert.True(responseItem.WorkItemId > 0);
        }

        [Fact]
        public void CreateExisting()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);
            WorkItem workItem = new WorkItem()
            {
                Title = "Create integration test",
                Description = "You need to test the database integration test with the DAO layer.",
                KanbanListId = 1
            };

            WorkItem responseItem = dao.Create(workItem);

            Assert.Throws<AlreadyExistsDaoException>(() =>
            {
                dao.Create(responseItem);
            });
        }

        [Fact]
        public void Delete()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);
            WorkItem workItem = new WorkItem()
            {
                Title = "Create integration test",
                Description = "You need to test the database integration test with the DAO layer.",
                KanbanListId = 1
            };

            WorkItem responseItem = dao.Create(workItem);
            dao.Delete(responseItem.WorkItemId);

            Assert.Throws<NotFoundDaoException>(() =>
            {
                dao.Delete(responseItem.WorkItemId);
            });
        }

        [Fact]
        public void DeleteNonExisting()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);
            const int workItem = 123;

            Assert.Throws<NotFoundDaoException>(() =>
            {
                dao.Delete(workItem);
            });
        }

        [Fact]
        public void Update()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);
            WorkItem workItem = new WorkItem()
            {
                Title = "Create integration test",
                Description = "You need to test the database integration test with the DAO layer.",
                KanbanListId = 1
            };

            WorkItem responseItem = dao.Create(workItem);

            WorkItem updateInformation = new WorkItem()
            {
                WorkItemId = responseItem.WorkItemId,
                Title = "Updated Title",
                Description = "Updated Description",
                KanbanListId = 2
            };

            WorkItem updatedItem = dao.Update(updateInformation.WorkItemId, updateInformation);

            Assert.Equal(updatedItem.WorkItemId, updateInformation.WorkItemId);
            Assert.Equal(updatedItem.Title, updateInformation.Title);
            Assert.Equal(updatedItem.Description, updateInformation.Description);
            Assert.Equal(updatedItem.KanbanListId, updateInformation.KanbanListId);
        }

        [Fact]
        public void UpdateNonExisting()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);

            WorkItem updateInformation = new WorkItem()
            {
                WorkItemId = 123,
                Title = "Updated Title",
                Description = "Updated Description",
                KanbanListId = 1
            };

            Assert.Throws<NotFoundDaoException>(() =>
            {
                dao.Update(updateInformation.KanbanListId, updateInformation);
            });
        }

        [Fact]
        public void Get()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);
            WorkItem workItem = new WorkItem()
            {
                Title = "Create integration test",
                Description = "You need to test the database integration test with the DAO layer.",
                KanbanListId = 1
            };

            WorkItem responseItem = dao.Create(workItem);

            WorkItem existingItem = dao.Get(responseItem.WorkItemId);

            Assert.Equal(existingItem.WorkItemId, workItem.WorkItemId);
            Assert.Equal(existingItem.Title, workItem.Title);
            Assert.Equal(existingItem.Description, workItem.Description);
            Assert.Equal(existingItem.KanbanListId, workItem.KanbanListId);
        }

        [Fact]
        public void GetNonExisting()
        {
            this.RecreateDatabase();
            WorkItemsDao dao = new WorkItemsDao(Configuration);
            const int workItemId = 123;

            Assert.Throws<NotFoundDaoException>(() =>
            {
                dao.Get(workItemId);
            });
        }
    }
}
