# Kanban Bootcamp

This is the backend code for the Bootcamp Kanban project.

## Prerequirements

* Visual Studio Community 2019 or Visual Studio Code
* .NET Core SDK 3.1

## How To Run

* Open solution in KanbanBootcamp.sln in Visual Studio
* Run the application (The application will be hosted at http://localhost:7000)

## Regenerating the Database Definition

* If possible remove the database file "kanban.db"
* Apply your changes in the class KanbanContext.cs in the Bootcamp.Kanban.Dao project
* Run the script "RecreateDatabase.bat" in the Bootcamp.Kanban.Dao folder

## Tests

* You can run the tests using Visual Studio or Visual Studio Code
* The solution only contains the test project "Bootcamp.Kanban.Dao.Tests"

## Usage from Visual Studio Code (If you are using VS2019 skip this)

* To run the application 
    ```batch
    dotnet run --project .\Bootcamp.Kanban.Controller\Bootcamp.Kanban.Controller.csproj
    ```
* To run the integration tests
    ```batch
    dotnet test
    ```