﻿using System;
namespace Bootcamp.Kanban.Dao
{
    public class DatabaseConfiguration
    {
        public string Name { get;}

        public DatabaseConfiguration(string name)
        {
            this.Name = name;
        }
    }
}
