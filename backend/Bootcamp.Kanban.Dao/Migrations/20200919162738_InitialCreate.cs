﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bootcamp.Kanban.Dao.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KanbanList",
                columns: table => new
                {
                    KanbanListId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KanbanList", x => x.KanbanListId);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Username = table.Column<string>(nullable: false),
                    FullName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Username);
                });

            migrationBuilder.CreateTable(
                name: "WorkItem",
                columns: table => new
                {
                    WorkItemId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    KanbanListId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkItem", x => x.WorkItemId);
                    table.ForeignKey(
                        name: "FK_WorkItem_KanbanList_KanbanListId",
                        column: x => x.KanbanListId,
                        principalTable: "KanbanList",
                        principalColumn: "KanbanListId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "KanbanList",
                columns: new[] { "KanbanListId", "Order", "Title" },
                values: new object[] { 1, 0, "Todo" });

            migrationBuilder.InsertData(
                table: "KanbanList",
                columns: new[] { "KanbanListId", "Order", "Title" },
                values: new object[] { 2, 1, "Doing" });

            migrationBuilder.InsertData(
                table: "KanbanList",
                columns: new[] { "KanbanListId", "Order", "Title" },
                values: new object[] { 3, 2, "Done" });

            migrationBuilder.InsertData(
                table: "WorkItem",
                columns: new[] { "WorkItemId", "Description", "KanbanListId", "Title" },
                values: new object[] { 100, "You need to implement the update method.", 1, "Update method" });

            migrationBuilder.InsertData(
                table: "WorkItem",
                columns: new[] { "WorkItemId", "Description", "KanbanListId", "Title" },
                values: new object[] { 101, "When you create a new WorkItem, the description is not being populated.", 1, "Description issue" });

            migrationBuilder.InsertData(
                table: "WorkItem",
                columns: new[] { "WorkItemId", "Description", "KanbanListId", "Title" },
                values: new object[] { 102, "Some backend errors are not being handled properly (Some HTTP Status code is expected.)", 2, "Backend error handling" });

            migrationBuilder.CreateIndex(
                name: "IX_WorkItem_KanbanListId",
                table: "WorkItem",
                column: "KanbanListId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "WorkItem");

            migrationBuilder.DropTable(
                name: "KanbanList");
        }
    }
}
