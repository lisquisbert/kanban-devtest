﻿using System;
using Bootcamp.Kanban.Dao.Exceptions;
using Bootcamp.Kanban.Domain;
using Microsoft.EntityFrameworkCore;

namespace Bootcamp.Kanban.Dao
{
    public class KanbanContext : DbContext
    {
        public string DatabaseName { get; }

        public DbSet<WorkItem> WorkItem { get; set; }
        public DbSet<KanbanList> KanbanList { get; set; }
        public DbSet<User> User { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options
                .UseLazyLoadingProxies()
                .UseSqlite($"Data Source={DatabaseName}");

        public KanbanContext()
        {
            this.DatabaseName = "kanban.db";
        }

        public KanbanContext(DatabaseConfiguration configuration)
        {
            this.DatabaseName = configuration.Name;

            try
            {
                this.Database.EnsureCreated();
            }
            catch (Exception err)
            {
                throw new ConnectionDaoException(err.Message, err);
            }
            
        }

        public void RecreateDatabase()
        {
            this.Database.EnsureDeleted();
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KanbanList>().HasData(new KanbanList { KanbanListId = 1, Title = "Todo", Order = 0 });
            modelBuilder.Entity<KanbanList>().HasData(new KanbanList { KanbanListId = 2, Title = "Doing", Order = 1 });
            modelBuilder.Entity<KanbanList>().HasData(new KanbanList { KanbanListId = 3, Title = "Done", Order = 2 });


            modelBuilder.Entity<WorkItem>().HasData(new WorkItem { WorkItemId = 100, Title = "Update method", Description = "You need to implement the update method.", KanbanListId = 1 });
            modelBuilder.Entity<WorkItem>().HasData(new WorkItem { WorkItemId = 101, Title = "Description issue", Description = "When you create a new WorkItem, the description is not being populated.", KanbanListId = 1 });
            modelBuilder.Entity<WorkItem>().HasData(new WorkItem { WorkItemId = 102, Title = "Backend error handling", Description = "Some backend errors are not being handled properly (Some HTTP Status code is expected.)", KanbanListId = 2 });
        }
    }
}
