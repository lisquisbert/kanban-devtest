﻿using Bootcamp.Kanban.Domain;
using Bootcamp.Kanban.Dao.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Bootcamp.Kanban.Dao
{
    public class WorkItemsDao
    {
        private DatabaseConfiguration Configuration { get; }

        public WorkItemsDao(DatabaseConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public WorkItem Create(WorkItem workItem)
        {
            KanbanContext context = new KanbanContext(Configuration);

            if (context.WorkItem.Find(workItem.WorkItemId) != null)
            {
                throw new AlreadyExistsDaoException($"The work item with id: {workItem.WorkItemId} already exist.");
            }

            context.Add(workItem);
            context.SaveChanges();

            return workItem;
        }

        public IEnumerable<WorkItem> GetAll()
        {
            KanbanContext context = new KanbanContext(Configuration);
            return context.WorkItem.ToList();
        }

        public WorkItem Update(int WorkItemId, WorkItem workItem)
        {
            KanbanContext context = new KanbanContext(Configuration);
            WorkItem existing = context.WorkItem.Find(WorkItemId);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The work item with id: {WorkItemId} doesn't exist.");
            }

            existing.Title = workItem.Title;
            existing.Description = workItem.Description;
            existing.KanbanListId = workItem.KanbanListId;

            context.Update(existing);
            context.SaveChanges();
            return existing;
        }

        public WorkItem Get(int workItemId)
        {
            KanbanContext context = new KanbanContext(Configuration);
            WorkItem existing = context.WorkItem.Find(workItemId);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The work item with id: {workItemId} doesn't exist.");
            }
            return existing;
        }

        public void Delete(int workItemId)
        {
            KanbanContext context = new KanbanContext(Configuration);
            WorkItem existing = context.WorkItem.Find(workItemId);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The work item with id: {workItemId} doesn't exist.");
            }
            context.Remove(existing);
            context.SaveChanges();
        }
    }
}
