﻿using System;
using System.Collections.Generic;
using System.Text;
using Bootcamp.Kanban.Domain;
using Bootcamp.Kanban.Dao.Exceptions;
using System.Security.Claims;
using System.Linq;

namespace Bootcamp.Kanban.Dao
{
    public class UsersDao
    {
        private DatabaseConfiguration Configuration { get; }
        public UsersDao(DatabaseConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        public User Update(string UserName, User user)
        {
            KanbanContext context = new KanbanContext(Configuration);
            User existing = context.User.Find(UserName);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The User: {UserName} doesn't exist.");
            }

            existing.Username = user.Username;
            existing.FullName = user.FullName;
            existing.Email = user.Email;

            context.Update(existing);
            context.SaveChanges();
            return existing;
        }
        public User Create(User user)
        {
            KanbanContext context = new KanbanContext(Configuration);

            if (context.User.Find(user.Username) != null)
            {
                throw new AlreadyExistsDaoException($"The user with username: {user.Username} already exist.");
            }

            context.Add(user);
            context.SaveChanges();

            return user;
        }
        public User Get(string userName)
        {
            KanbanContext context = new KanbanContext(Configuration);
            User existing = context.User.Find(userName);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The work item with id: {userName} doesn't exist.");
            }
            return existing;
        }
        public IEnumerable<User> GetAll()
        {
            KanbanContext context = new KanbanContext(Configuration);
            return context.User.ToList();
        }
        public void Delete(string UserName)
        {
            KanbanContext context = new KanbanContext(Configuration);
            User existing = context.User.Find(UserName);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The User: {UserName} doesn't exist.");
            }
            context.Remove(existing);
            context.SaveChanges();
        }
    }
}
