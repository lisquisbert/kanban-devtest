﻿using System;
namespace Bootcamp.Kanban.Dao.Exceptions
{
    public class NotFoundDaoException : Exception
    {
        public NotFoundDaoException()
        {
        }

        public NotFoundDaoException(string message)
            : base(message)
        {
        }

        public NotFoundDaoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
