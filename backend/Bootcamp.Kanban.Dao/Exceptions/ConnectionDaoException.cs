﻿using System;
namespace Bootcamp.Kanban.Dao.Exceptions
{
    public class ConnectionDaoException : Exception
    {
        public ConnectionDaoException()
        {
        }

        public ConnectionDaoException(string message)
            : base(message)
        {
        }

        public ConnectionDaoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
