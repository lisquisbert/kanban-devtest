﻿using System;
namespace Bootcamp.Kanban.Dao.Exceptions
{
    public class AlreadyExistsDaoException : Exception
    {
        public AlreadyExistsDaoException()
        {
        }

        public AlreadyExistsDaoException(string message)
            : base(message)
        {
        }

        public AlreadyExistsDaoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
