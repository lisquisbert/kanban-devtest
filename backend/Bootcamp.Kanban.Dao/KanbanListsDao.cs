﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bootcamp.Kanban.Dao.Exceptions;
using Bootcamp.Kanban.Domain;

namespace Bootcamp.Kanban.Dao
{
    public class KanbanListsDao
    {
        private DatabaseConfiguration Configuration { get; }

        public KanbanListsDao(DatabaseConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        
        public KanbanList Create(KanbanList kanbanList)
        {
            KanbanContext context = new KanbanContext(Configuration);

            if (context.KanbanList.Find(kanbanList.KanbanListId) != null)
            {
                throw new AlreadyExistsDaoException($"The kanbanList with id: {kanbanList.KanbanListId} already exist.");
            }

            context.Add(kanbanList);
            context.SaveChanges();

            return kanbanList;
        }
        
        public IEnumerable<KanbanList> GetAll()
        {
            KanbanContext context = new KanbanContext(Configuration);
            return context.KanbanList.ToList();
        }

        public KanbanList Get(int kanbanListId)
        {
            KanbanContext context = new KanbanContext(Configuration);
            KanbanList existing = context.KanbanList.Find(kanbanListId);
            if (existing == null)
            {
                throw new NotFoundDaoException($"The kanbanList with id: {kanbanListId} doesn't exist.");
            }
            return existing;
        }
    }
}
