﻿using System;
using Bootcamp.Kanban.Dao.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Bootcamp.Kanban.Controller.Controllers
{
    public class CustomController <TController> : ControllerBase
    {
        protected DaoProvider DaoProvider { get; }
        protected readonly ILogger<TController> _logger;

        public CustomController(ILogger<TController> logger)
        {
            _logger = logger;
            DaoProvider = new DaoProvider();
        }

        protected IActionResult ProcessRequest(Func<IActionResult> request)
        {
            try
            {
                return request();
            }
            catch (ConnectionDaoException)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            catch (NotFoundDaoException)
            {
                return NotFound();
            }
            catch (AlreadyExistsDaoException err)
            {
                return Conflict(err.Message);
            }
            catch (Exception)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
