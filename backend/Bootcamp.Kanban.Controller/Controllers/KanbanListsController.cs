﻿using System.Collections.Generic;
using Bootcamp.Kanban.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Bootcamp.Kanban.Controller.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class KanbanListsController : CustomController<KanbanListsController>
    {
        public KanbanListsController(ILogger<KanbanListsController> logger)
            : base(logger)
        {
        }
        
        [HttpGet]
        public IActionResult GetAll()
        {
            return ProcessRequest(() =>
            {
                IEnumerable<KanbanList> all = DaoProvider.KanbanLists.GetAll();
                return Ok(all);
            });
        }

        [HttpGet("{KanbanListId}", Name = "GetKanbanList")]
        public IActionResult GetKanbanList(int KanbanListId)
        {
            return ProcessRequest(() =>
            {
                KanbanList existing = DaoProvider.KanbanLists.Get(KanbanListId);
                return Ok(existing);
            });
        }

        [HttpPost]
        public IActionResult Create(KanbanList KanbanList)
        {
            return ProcessRequest(() =>
            {
                KanbanList newKanbanList = DaoProvider.KanbanLists.Create(KanbanList);
                return CreatedAtRoute("GetKanbanList", new { newKanbanList.KanbanListId }, newKanbanList);
            });
        }
        
    }
}
