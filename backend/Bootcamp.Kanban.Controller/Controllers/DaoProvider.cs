﻿using System;
using Bootcamp.Kanban.Dao;
using Bootcamp.Kanban.Domain;

namespace Bootcamp.Kanban.Controller.Controllers
{
    public class DaoProvider
    {
        DatabaseConfiguration databaseConfiguration;

        public WorkItemsDao WorkItems { get; }
        public KanbanListsDao KanbanLists { get; }
        public UsersDao Users { get; }
        public DaoProvider()
        {
            this.databaseConfiguration = new DatabaseConfiguration("kanban.db");
            this.WorkItems = new WorkItemsDao(databaseConfiguration);
            this.KanbanLists = new KanbanListsDao(databaseConfiguration);
            this.Users = new UsersDao(databaseConfiguration);
        }
    }
}
