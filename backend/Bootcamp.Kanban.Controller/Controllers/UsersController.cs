﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Bootcamp.Kanban.Domain;
using Microsoft.Extensions.Logging;
using Castle.Core.Internal;

namespace Bootcamp.Kanban.Controller.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : CustomController<UsersController>
    {
        public UsersController(ILogger<UsersController> logger) : base(logger)
        {

        }
        [HttpPost]
        public IActionResult Create(User User)
        {
            return ProcessRequest(() =>
            {
                User newUser = DaoProvider.Users.Create(User);
                return CreatedAtRoute("GetUser", new { newUser.Username }, newUser);
            });
        }
        [HttpGet("{UserName}", Name = "GetUser")]
        public IActionResult GetUser(string Username)
        {
            return ProcessRequest(() =>
            {
                User existing = DaoProvider.Users.Get(Username);
                return Ok(existing);
            });
        }

        [HttpGet]
        public IActionResult Get()
        {
            return ProcessRequest(() =>
            {
                IEnumerable<User> all = DaoProvider.Users.GetAll();
                return Ok(all);
            });
        }
        [HttpPut]
        [Route("{UserName}")]
        public IActionResult Update(
            [FromRoute] string UserName,
            [FromBody] User user)
        {
            return ProcessRequest(() =>
            {
                return Ok(DaoProvider.Users.Update(UserName, user));
            });
        }
        [HttpDelete]
        [Route("{UserName}")]
        public IActionResult Delete(string UserName)
        {
            return ProcessRequest(() =>
            {
                DaoProvider.Users.Delete(UserName);
                return Ok();
            });
        }
    }
}
