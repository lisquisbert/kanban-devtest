﻿using System.Collections.Generic;
using Bootcamp.Kanban.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Bootcamp.Kanban.Controller.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkItemsController : CustomController <WorkItemsController>
    {
        public WorkItemsController(ILogger<WorkItemsController> logger)
            : base(logger)
        {
        }

        [HttpGet]
        public IActionResult Get()
        {
            return ProcessRequest(() =>
            {
                IEnumerable<WorkItem> all = DaoProvider.WorkItems.GetAll();
                return Ok(all);
            });
        }

        [HttpGet("{WorkItemId}", Name = "GetWorkItem")]
        public IActionResult GetWorkItem(int WorkItemId)
        {
            return ProcessRequest(() =>
            {
                WorkItem existing = DaoProvider.WorkItems.Get(WorkItemId);
                return Ok(existing);
            });
        }

        [HttpPost]
        public IActionResult Create(WorkItem WorkItem)
        {
            return ProcessRequest(() =>
            {
                WorkItem newWorkItem = DaoProvider.WorkItems.Create(WorkItem);
                return CreatedAtRoute("GetWorkItem", new { newWorkItem.WorkItemId }, newWorkItem);
            });
        }

        [HttpPut]
        [Route("{WorkItemId}")]
        public IActionResult Update(
            [FromRoute] int WorkItemId,
            [FromBody] WorkItem WorkItem)
        {
            return ProcessRequest(() =>
            {
                return Ok(DaoProvider.WorkItems.Update(WorkItemId, WorkItem));
            });
        }

        [HttpDelete]
        [Route("{WorkItemId}")]
        public IActionResult Delete(int WorkItemId)
        {
            return ProcessRequest(() =>
            {
                DaoProvider.WorkItems.Delete(WorkItemId);
                return Ok();
            });
        }
    }
}
