# How start with a Challenge

First you will need to create a new branch, this branch should have your name.

__Note: All your code should come in this branch__

```bash
git checkout -b [full_name]
```


After finishing a challenge you will need to make a commit with the following format.

```bash
git commit -m "[ChallengeName] - [Short description about your changes]"
```

Push the changes to the main repository

```bash
git push origin [fullname]
```
