# Cloning the Code
* Click Clone button and copy the `repoUrl` from HTTPS tab.
![alt](./img/github.png)

* Open a **CMD** and type `git clone [repoUrl]`
![alt](./img/clone_cmd.png)

* This will ask for your credentials.
![alt](./img/github_login.png)

* Configure your Name and Email with
```bash
git config --global user.name "Full Name"
git config --global user.email email@email.com
```

And will have the code ready to build and work.

## Common Problems

### Github Credencials

* Open the **Credentials Manager**
![alt](./img/credentials_windows.png)
* Select **Windows Credentials**
![alt](./img/credentials_w.png)
* Remove all credentials related to **github**
![alt](./img/credencials_tres.png)