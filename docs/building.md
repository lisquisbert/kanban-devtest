# Building and Running

## Frontend

### Installing yarn

You can get yarn [here](https://yarnpkg.com/)

Run the following command to get the dependencies.

```bash
cd frotend
yarn install
```

Run the following command to run the frontend app.

```bash
yarn start
```

## Backend
To build the backend code you need to have installed `dotnet 3.1`, if you have **Visual Studio 2019** this is already installed.

Try the next command to check your `dotnet` installation.

```bash
dotnet --list-sdks

#3.1.100 [C:\Program Files\dotnet\sdk]
```
Now you can build and run the project with VS or VsCode

### Visual Studio Code

If you want to use **Visual Studio Code** you need to install the [C# Plugin](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp), then you need tu run the following command first to restore the dependencies and second to run the app throught the console.

```bash
cd backend
dotnet restore
```

```bash
dotnet run -p Bootcamp.Kanban.Controller/Bootcamp.Kanban.Controller.csproj
```
#### Debugging with VSCode

Fist of all create the `.vscode` folder and the `launch.json` file.

```
cd backend
mkdir .vscode
```

Copy the following in a file called `launch.json`

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": ".NET Core Launch (web)",
            "type": "coreclr",
            "request": "launch",
            "preLaunchTask": "build",
            "program": "${workspaceFolder}/Bootcamp.Kanban.Controller/bin/Debug/netcoreapp3.1/Bootcamp.Kanban.Controller.dll",
            "args": [],
            "cwd": "${workspaceFolder}/Bootcamp.Kanban.Controller",
            "stopAtEntry": false,
            "serverReadyAction": {
                "action": "openExternally",
                "pattern": "\\bNow listening on:\\s+(https?://\\S+)"
            },
            "env": {
                "ASPNETCORE_ENVIRONMENT": "Development"
            },
            "sourceFileMap": {
                "/Views": "${workspaceFolder}/Views"
            }
        },
        {
            "name": ".NET Core Attach",
            "type": "coreclr",
            "request": "attach",
            "processId": "${command:pickProcess}"
        }
    ]
}
```

Once you had this file, you can use the to press the `.Net Core Launch` button located in status bar, then select the `.Net Core Launch (web)` option.

![run botton ](./img/vscode.PNG)

### Visual Studio
You need to open the __KanbanBootcamp.sln__ file with Visual Studio.

Right click on `Bootcamp.Kanban.Controller` project and select it as startup project.


![alt](./img/vsright.PNG)

To run the project click the run button.

![image   ](./img/vsrun.PNG)